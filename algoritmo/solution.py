
"""
Alejandro Granda: Prueba teorica

Escribir un algoritmo que valide una identificación: 
-Si tiene 10 digitos es Cedula.
-Si tiene 13 digitos es RUC.
-Si empieza con 09 es de Guayaquil, caso contrario otra provincia.
-Si no cumple con lo anterior se debe mostrar que la cedula no es valida.


PseudoCodigo>

Inicio
Leer identificación
Si longitud de identificación es igual a 10 entonces
Mostrar "Cedula"
Si los dos primeros dígitos son 09 entonces
Mostrar "De Guayaquil"
Sino
Mostrar "De otra provincia"
Si longitud de identificación es igual a 13 entonces
Mostrar "RUC"
Si los dos primeros dígitos son 09 entonces
Mostrar "De Guayaquil"
Sino
Mostrar "De otra provincia"
Sino
Mostrar "Identificación inválida"
Fin Si
Fin

"""

# Definimos la función que validará la identificación

def validar_identificacion(identificacion):
    # Validamos si la identificación tiene 10 dígitos, es decir, si es una cédula
    if len(identificacion) == 10:
        # Si es una cédula, imprimimos "Cedula" en la consola
        print("Cedula", end=" ")
        # Obtenemos los dos primeros dígitos de la identificación, que representan la provincia de emisión
        provincia = identificacion[0:2]
        # Comprobamos si la provincia es alguna de las cinco primeras provincias (01 a 05)
        if provincia == "01" or provincia == "02" or provincia == "03" or provincia == "04" or provincia == "05":
            # Si la provincia es alguna de las cinco primeras, la identificación es de otra provincia
            print("de otra provincia.")
        # Comprobamos si la provincia es Guayaquil (código 09)
        elif provincia == "09":
            # Si la provincia es Guayaquil, la identificación es de Guayaquil
            print("de Guayaquil.")
        else:
            # Si la provincia no es ninguna de las anteriores, la identificación es de otra provincia
            print("de otra provincia.")
    # Validamos si la identificación tiene 13 dígitos, es decir, si es un RUC
    elif len(identificacion) == 13:
        # Si es un RUC, imprimimos "RUC" en la consola
        print("RUC", end=" ")
        # Comprobamos si los dos primeros dígitos de la identificación son "09", que indica que es de Guayaquil
        if identificacion[0:2] == "09":
            # Si es de Guayaquil, imprimimos "de Guayaquil" en la consola
            print("de Guayaquil.")
        else:
            # Si no es de Guayaquil, imprimimos "de otra provincia" en la consola
            print("de otra provincia.")
    else:
        # Si la identificación no tiene 10 ni 13 dígitos, es una identificación inválida
        print("Identificación invalida.")


print("0900000001: ")
validar_identificacion("0900000001") # Cedula de Guayaquil.

print("09001: ")
validar_identificacion("09001") # Identificación invalida.

print("0900000001001: ")
validar_identificacion("0900000001001") # RUC de Guayaquil.

print("090000000000001: ")
validar_identificacion("090000000000001") # Identificación invalida.

print("1700000001: ")
validar_identificacion("1700000001") # Cedula de otra provincia.

print("1700000001001: ")
validar_identificacion("1700000001001") # RUC de otra provincia.


# Nota 
"""
Los cinco primeros dígitos de la cédula, es decir, los códigos de provincia del 01 al 05, son asignados de 
manera única a las cinco primeras provincias de Ecuador en orden alfabético: Azuay, Bolívar, Cañar, Carchi 
y Chimborazo. Sin embargo, otros rangos de dígitos de cédula pueden ser asignados a varias provincias. Por 
ejemplo, el código de provincia 09 es asignado a Guayas, Santa Elena, Los Ríos y Galápagos.

Por lo tanto, si se quiere una validación más precisa, se debería utilizar una base de datos actualizada 
de códigos de provincia asignados a rangos de dígitos de cédula, en lugar de solo verificar los primeros 
dos dígitos de la cédula.

"""